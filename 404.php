<?php
get_header();
?>
    <div class="section noPadding whiteBG page-header">
        <div class="lamour-404">
            <div class="container">
                <h1>404 Page Not Found</h1>
                <h3>The page you requested was not found</h3>
            </div>
        </div>
    </div>


<?php
get_footer();
