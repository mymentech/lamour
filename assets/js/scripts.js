;(function ($) {
    $(document).ready(function () {
        autoSlide();



        // $('a[href$="/subscribe"]').on('click', function(){
        //     if('logged_out'===dynamic_data.log_in_status){
        //         location.href = "/registration";
        //         return false;
        //     }
        // });

        if ('logged_in' === dynamic_data.log_in_status) {
            $('#profile-btn').css({"color":"green","border": "4px solid green"});
        }


    });
    $('#subButton').on('click', function () {
        var email = $(this).parent().find('input#email').val();
        var nonce = $(this).parent().find('input#_wpnonce').val();

        if ('' === email) {
            alert("Please enter your E-mail address.");
            return false
        }
        if (!validateEmail(email)) {
            alert('"' + email + '"' + ' not a valid email.');
            return false;
        }

        $.post(dynamic_data.admin_url, {
            "action": "lamour_mailchimp_email",
            "email_address": email,
            "lamour_ref": nonce,
        }, function (data) {
            alert(data);

        });

        $(this).parent().find('input#email').val('');
        return false;
        //validateEmail(email);
    });


    $('.modal-pop').magnificPopup({
        type: 'inline',
        removalDelay: 500,
        midClick: true,
        mainClass: 'mfp-fade mfp-table'
    });

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $(".chzn-select").chosen({disable_search_threshold: 5});

    $(document).ready(function () {
        init_alert();
    });

    /*----------CONSTANTS----------*/
    function init_alert() {
        sitewide(this);
    }

    function sitewide(vars) {
        $("#alert .close").click(function () {
            $("#alert").slideUp();
        });

        //Make elements with this tag have full height of page
        var windowHeight = $(window).height();
        $('.fullHeight, .fullHeightMin').css("height", windowHeight);
        $('.fullHeightMin').css("min-height", windowHeight);
        $(window).resize(function () {
            var windowHeight = $(window).height();
            $('.fullHeight, .fullHeightMin').css("height", windowHeight);
        });
    }


    //Create new wheel object specifying the parameters at creation time.
    var theWheel = new Winwheel({
        'numSegments': 14,     // Specify number of segments.
        'outerRadius': 210,   // Set outer radius so wheel fits inside the background.
        'textFontSize': 13,    // Set font size as desired.
        'textFontWeight': 'bold',
        'textFontFamily': 'sans-serif',
        'textAlignment': 'outer',
        'textMargin': 15,
        'lineWidth': 3,
        'segments':        // Define segments including colour and text.
            [
                {'fillStyle': '#305bb3', 'text': 'FREE Shipping', 'textFillStyle': '#fff'},
                {'fillStyle': '#268c3c', 'text': '10% OFF Anything', 'textFillStyle': '#fff'},
                {'fillStyle': '#268c3c', 'text': '10% OFF Anything', 'textFillStyle': '#fff'},
                {'fillStyle': '#268c3c', 'text': '10% OFF Anything', 'textFillStyle': '#fff'},
                {'fillStyle': '#121212', 'text': '1 FREE BOX', 'textFillStyle': '#fff'},
                {'fillStyle': '#a426b3', 'text': '15% OFF Anything', 'textFillStyle': '#fff'},
                {'fillStyle': '#268c3c', 'text': '10% OFF Anything', 'textFillStyle': '#fff'},
                {'fillStyle': '#eac533', 'text': 'No Luck Today', 'textFillStyle': '#121212'},
                {'fillStyle': '#268c3c', 'text': '10% OFF Anything', 'textFillStyle': '#fff'},
                {'fillStyle': '#305bb3', 'text': 'FREE Shipping', 'textFillStyle': '#fff'},
                {'fillStyle': '#305bb3', 'text': 'FREE Shipping', 'textFillStyle': '#fff'},
                {'fillStyle': '#268c3c', 'text': '10% OFF Anything', 'textFillStyle': '#fff'},
                {'fillStyle': '#a426b3', 'text': '15% OFF Anything', 'textFillStyle': '#fff'},
                {'fillStyle': '#a426b3', 'text': '15% OFF Anything', 'textFillStyle': '#fff'},
            ],
        'animation':           // Specify the animation to use.
            {
                'type': 'spinToStop',
                'duration': 5,     // Duration in seconds.
                'spins': 8,     // Number of complete spins.
                'callbackFinished': 'alertPrize()'
            }
    });

    // Vars used by the code in this page to do power controls.
    var wheelPower = 2;
    var wheelSpinning = false;

    setTimeout(function () {
        $('#bt-menu').addClass('bt-menu-open');
    }, 3000);


})(jQuery);


//Initializing Slider
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
    showDivs(slideIndex += n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("mySlides");
    if (x.length === 0) {
        return false;
    }
    if (n > x.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = x.length
    }
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    x[slideIndex - 1].style.display = "block";
}

function autoSlide() {
    setInterval(function () {
        plusDivs(1);
    }, 5000);
}