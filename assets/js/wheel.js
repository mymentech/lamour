function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

// -------------------------------------------------------
// Click handler for spin button.
// -------------------------------------------------------
;(function ($) {
    function startSpin() {
        // Ensure that spinning can't be clicked again while already running.
        email = $('#wheel-email').val();
        if (!email || !validateEmail(email)) {
            $('#wheel-email').addClass("bad-email animated bounce");
        } else if (wheelSpinning == false) {
            $('#wheel-email').removeClass("bad-email animated bounce");
            // Based on the power level selected adjust the number of spins for the wheel, the more times is has
            // to rotate with the duration of the animation the quicker the wheel spins.
            if (wheelPower == 1) {
                theWheel.animation.spins = 3;
            }
            else if (wheelPower == 2) {
                theWheel.animation.spins = 8;
            }
            else if (wheelPower == 3) {
                theWheel.animation.spins = 15;
            }

            // Disable the spin button so can't click again while wheel is spinning.
            $('#spin_button, #close_wheel').addClass("animated flipOutY");

            // Begin the spin animation by calling startAnimation on the wheel object.
            theWheel.startAnimation();

            // Set to true so that power can't be changed and spin button re-enabled during
            // the current animation. The user will have to reset before spinning again.
            wheelSpinning = true;
        }
    }

// -------------------------------------------------------
// Function for reset button.
// -------------------------------------------------------
    function resetWheel() {
        theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
        theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
        theWheel.draw();                // Call draw to render changes to the wheel.

        wheelSpinning = false;          // Reset to false to power buttons and spin can be clicked again.
    }

//-------------------------------------------------------
//Function for closing the wheel and adding cookie
//-------------------------------------------------------
    function cancelWheel() {
        $('#bt-menu').fadeOut();
        setTimeout(function () {
            $('#bt-menu').remove();
        }, 2500);

        //do a post to the backend to add a cookie
    }

// -------------------------------------------------------
// Called when the spin animation has finished by the callback feature of the wheel because I specified callback in the parameters.
// -------------------------------------------------------
    function alertPrize() {
        // Get the segment indicated by the pointer on the wheel background which is at 0 degrees.
        var winningSegment = theWheel.getIndicatedSegment();
        var winningSegmentNumber = theWheel.getIndicatedSegmentNumber();

        // Loop and set fillStyle of all segments to gray.
        for (var x = 1; x < theWheel.segments.length; x++) {
            theWheel.segments[x].fillStyle = '#333';
            theWheel.segments[x].textFillStyle = '#fff';
        }

        // Make the winning one yellow.
        theWheel.segments[winningSegmentNumber].fillStyle = '#cd2d2d';
        theWheel.segments[winningSegmentNumber].textFillStyle = '#fff';

        // Call draw function to render changes.
        theWheel.draw();
        $('#spin_button, #close_wheel').addClass("hide");
        $('#continue_wheel').removeClass("hide");

        //send number to backend, if won, save promocode cookie and return text to header and to wheel area
        email = $('#wheel-email').val();
        $.post("/actions/save_promocode", {id: winningSegmentNumber, email: email}).done(function (data) {
            data = JSON.parse(data);
            $('#wheel-text-2').html(data.text);
            if (data.option != 2) {
                $('#wheel-text').html("You have won <b class='black'>" + winningSegment.text + "</b>");
                $('#wheel-email').attr('disabled', true);
                $('#wheel-email').val(data.code);
            } else {
                $('#wheel-text').html("Well... <b class='black'>" + winningSegment.text + "</b>");
                $('#wheel-email').fadeOut();
            }
        });

    }
})(jQuery);