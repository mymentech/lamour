<?php
/**
 * Created by PhpStorm.
 * User: mozammel
 * Date: 9/2/18
 * Time: 11:53 PM
 */

class Mymentech
{
    public function __construct()
    {


        add_action('after_setup_theme', array($this, 'lamour_theme_setup'));
        add_action('init', array($this, 'load_cs_framework'));
        remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);
    }



    function lamour_theme_setup()
    {
        load_textdomain('lamour', get_theme_file_path('/languages'));

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        add_theme_support('title-tag');
        add_theme_support('woocommerce');
        //add_theme_support('wc-product-gallery-zoom');
        add_theme_support('wc-product-gallery-lightbox');
        add_theme_support('wc-product-gallery-slider');

        add_theme_support('post-thumbnails');
        add_theme_support('custom-header');


        // Registering Header Menu
        register_nav_menus(array(
            'main-menu' => esc_html__('Main Menu', 'lamour'),

        ));

        // Registering Header Menu
        register_nav_menus(array(
            'footer-menu' => esc_html__('Footer Menu', 'lamour'),

        ));

    }

    function load_cs_framework()
    {
        CSFramework_Metabox::instance(array());
    }


}

new Mymentech;