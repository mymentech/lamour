<div id="footer" class="section">
    <img src="<?php echo get_the_footer_logo() ?>" id="emblem"/>

    <div class="container">
        <?php print_lamour_footer_menu_items('footer-menu') ?>
        <div class="clear2"></div>
        <div class="divider"></div>
        <div class="clear2"></div>

        <div class="floatR" id="copyright">
            <?php echo cs_get_option('copyright_text') ?>
        </div>

        <div id="social" class="floatL">
            <a href="<?php echo esc_url(cs_get_option('lamour_facebook')) ?>"
               target="_blank"><i class="fa fa-facebook fa-inverse"></i></a>
            <a href="<?php echo esc_url(cs_get_option('lamour_twitter')) ?>" target="_blank"><i class="fa fa-twitter fa-inverse"></i></a>


            <a href="<?php echo esc_url(cs_get_option('lamour_gplus')) ?>"
               target="_blank"><i class="fa fa-google-plus fa-inverse"></i></a>


            <a href="mailto:<?php echo cs_get_option('lamour_email') ?>" target="_blank"><i class="fa fa-envelope fa-inverse"></i></a>


        </div>

    </div>
</div>

<!-- Account Modal -->
<div id="account-modal" class="white-popup mfp-hide">
    <div class="popup-header words4"><?php _e("Login or Create An Account", "lamour") ?></div>
    <div class="container text">
        <div class="half floatL">
            <?php

            if (is_user_logged_in()){
                printf('<div class="title">%s</div>', __("Your are already Logged In","lamour"));
                printf('<a href="%s"><div class="btn blackBG white">%s</div></a>',wp_logout_url(),__("Log Out",'Lamour'));
            }else{
                printf('<div class="title">%s</div>',__('Login','lamour'));
                wp_login_form(array(
                    'id_submit' => 'popup-submit',
                ));
            }

            ?>

        </div>

        <div class="half floatR">
            <?php if(!is_user_logged_in()): ?>
            <div class="title"><?php _e('Sign Up', 'lamour') ?></div>
            <?php _e("Create a free account to start your subscription, receive exclusive member deals, store your information,
            view and track your orders in your account, and much more.", "lamour") ?>
            <div class="clear"></div>
            <a href="<?php echo site_url('/registration') ?>">
                <div class="btn blackBG white"><?php _e("Register Now", "lamour") ?></div>
            </a>

            <?php endif ?>

        </div>

    </div>
</div>

<?php wp_footer() ?>

</body>
</html>