<?php
include_once get_theme_file_path('/includes/load.php');

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);



function lamor_title(){
    printf("<span class=\"title-script red\">%s</span>", get_the_title());
}

add_action('woocommerce_single_product_summary', 'lamor_title',5);

function lamor_desc(){

    echo '<span class="text">'.get_the_content().'</span>';
}

add_action('woocommerce_single_product_summary', 'lamor_desc',9);



function lamor_quantity(){

    echo '<label class="text alt">Quantity:</label>';
}

add_action('woocommerce_single_product_summary', 'lamor_quantity',11);

add_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
add_action( 'lamour_woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

function lamour_register_url(){
	return site_url().'/registration';
}

add_filter('register_url','lamour_register_url');
