<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=500, maximum-scale=1, user-scalable=no, target-densitydpi=device-dpi"/>
    <meta name="keywords"
          content="lamour, secret, sex, sexy, lingerie, clothes, passion, love, relationship, kiss, gift "/>
    <meta name="description"
          content="Lamour Secret, available in a room near you"/>
    <meta property="og:site_name" content="lamoursecret.com">
    <meta property="og:title"
          content="Lamour Secret">
    <meta property="og:url" content="http://lamoursecret.com">
    <meta property="og:description" content="Lamour Secret, available in a room near you">
    <meta property="og:type" content="website">
    <meta property="og:image" content="images/general/logo_black.png">

    <?php wp_head() ?>
    <link rel="shortcut icon" href="<?php echo header_site_icon() ?>">

</head>
<body <?php body_class() ?>>

<script type="text/javascript">var ssaUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'centro.pixel.ad/iap/ce6df34020c1db65';
    new Image().src = ssaUrl;</script>



<div id="header">


    <?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

        $count = WC()->cart->cart_contents_count;
        ?><a class="fa fa-shopping-cart" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>" id="shopping-btn"><?php
        if ( $count > 0 ) {
            ?>
            <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
            <?php
        }
        ?></a>

    <?php } ?>

<!--    <a href="/cart"><i class="fa fa-shopping-cart" id="shopping-btn"></i></a>-->
    <a href="#account-modal" class="modal-pop"><i class="fa fa-user" id="profile-btn"></i></a>









    <div class="container textCenter">
        <a href="<?php echo site_url() ?>"><img src="<?php echo get_the_header_logo() ?>" id="logo"/></a>
        <div class="clear"></div>
        <span class="header-text"><?php bloginfo('description') ?></span>
        <div class="clear2"></div>

        <div id="menu">
            <?php print_lamour_menu_items('main-menu') ?>
        </div>
    </div>
</div>