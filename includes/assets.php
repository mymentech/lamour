<?php

function lamour_scripts() {
	//CSS
	define( 'VERSION', time() );
	wp_enqueue_style( 'font-awesome', get_theme_file_uri( '/assets/css/font-awesome.min.css' ), null, VERSION );
	wp_enqueue_style( 'chosen-css', get_theme_file_uri( '/assets/css/chosen.css' ), null, '4.1.0' );
	wp_enqueue_style( 'magnific-popup-css', get_theme_file_uri( '/assets/css/magnific-popup.css' ), null, VERSION );
	wp_enqueue_style( 'animate-css', get_theme_file_uri( '/assets/css/animate.css' ), null, VERSION );
	wp_enqueue_style( 'style5-css', get_theme_file_uri( '/assets/css/style5.css' ), null, VERSION );
	wp_enqueue_style( 'shop-css', get_theme_file_uri( '/assets/css/shop.css' ), null, VERSION );
	wp_enqueue_style( 'layout-css', get_theme_file_uri( '/assets/css/layout.css' ), null, VERSION );
	wp_enqueue_style( 'google-fonts-css', '//fonts.googleapis.com/css?family=Lato:300,400,700|Kaushan+Script|Satisfy', null, VERSION );


	wp_enqueue_style( 'cs-skin-boxes-css', get_theme_file_uri( '/assets/css/cs-skin-boxes.css' ), null, VERSION );
	wp_enqueue_style( 'cs-select-css', get_theme_file_uri( '/assets/css/cs-select.css' ), null, VERSION );
	wp_enqueue_style( 'component-css', get_theme_file_uri( '/assets/css/component.css' ), null, VERSION );
	wp_enqueue_style( 'normalize-css', get_theme_file_uri( '/assets/css/normalize.css' ), null, VERSION );


	wp_enqueue_style( 'lamour-css', get_stylesheet_uri(), null, VERSION );

	//Scripts
	wp_enqueue_script( 'jquery-countdown-js', get_theme_file_uri( '/assets/js/jquery.countdown.js' ), array( 'jquery' ), VERSION, false );
	wp_enqueue_script( 'chosen-js', get_theme_file_uri( '/assets/js/chosen.jquery.js' ), array( 'jquery' ), VERSION, false );
	wp_enqueue_script( 'chosen-js', get_theme_file_uri( '/assets/js/chosen.jquery.js' ), array( 'jquery' ), VERSION );

	wp_enqueue_script( 'chosen-js', get_theme_file_uri( '/assets/js/chosen.jquery.js' ), array( 'jquery' ), VERSION, false );
	wp_enqueue_script( 'magnific-popup-js', get_theme_file_uri( '/assets/js/jquery.magnific-popup.min.js' ), array( 'jquery' ), VERSION, false );
	wp_enqueue_script( 'classie-js', get_theme_file_uri( '/assets/js/classie.js' ), array( 'jquery' ), VERSION, false );
	wp_enqueue_script( 'modernizr-custom-js', get_theme_file_uri( '/assets/js/modernizr.custom.js' ), array( 'jquery' ), VERSION, false );
	wp_enqueue_script( 'Winwheel-js', get_theme_file_uri( '/assets/js/Winwheel.js' ), array( 'jquery' ), VERSION, false );
	wp_enqueue_script( 'tweenmax-js', get_theme_file_uri( '/assets/js/TweenMax.min.js' ), array( 'jquery' ), VERSION, false );
	wp_enqueue_script( 'wheel-js', get_theme_file_uri( '/assets/js/TweenMax.min.js' ), array( 'jquery' ), VERSION, false );


	/**
	 * Lamour Registration related Scripts
	 */


	wp_enqueue_script( 'selectFx-js', get_theme_file_uri( '/assets/js/selectFx.js' ), array( 'jquery' ), VERSION, false );
	wp_enqueue_script( 'fullscreenForm-js', get_theme_file_uri( '/assets/js/fullscreenForm.js' ), array( 'jquery' ), VERSION, false );
	$registration_script = <<< REGI
(function() {
            var formWrap = document.getElementById( 'fs-form-wrap' );
            [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
                new SelectFx( el, {
                    stickyPlaceholder: false,
                    onChange: function(val){
                        document.querySelector('span.cs-placeholder').style.backgroundColor = val;
                    }
                });
            } );
            new FForm( formWrap, {
                onReview : function() {
                    classie.add( document.body, 'overview' ); // for demo purposes only
                }
            } );
        })();
        
REGI;


	/*13-9948*/


	wp_enqueue_script( 'lamour-scripts', get_theme_file_uri( '/assets/js/scripts.js' ), array(
		'jquery',
		'selectFx-js',
		'fullscreenForm-js'
	), VERSION, true );

	wp_add_inline_script( 'lamour-scripts', $registration_script );
	$log_in_status = is_user_logged_in()?'logged_in':'logged_out';

	$dynamic_data = array(
		'admin_url' => admin_url( 'admin-ajax.php' ),
		'log_in_status'=>$log_in_status
	);



	wp_localize_script( 'lamour-scripts', 'dynamic_data', $dynamic_data );

}

add_action( 'wp_enqueue_scripts', 'lamour_scripts' );
