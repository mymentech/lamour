<?php

function lamour_register_my_cpts_section() {

    /**
     * Post Type: Section.
     */

    $labels = array(
        "name"          => __( "Section", "lamour" ),
        "singular_name" => __( "Sections", "lamour" ),
    );

    $args = array(
        "label"               => __( "Section", "lamour" ),
        "labels"              => $labels,
        "description"         => "",
        "public"              => false,
        "publicly_queryable"  => false,
        "show_ui"             => true,
        "show_in_rest"        => false,
        "rest_base"           => "",
        "has_archive"         => false,
        "show_in_menu"        => true,
        "show_in_nav_menus"   => true,
        "exclude_from_search" => true,
        "capability_type"     => "post",
        "map_meta_cap"        => true,
        "hierarchical"        => false,
        "rewrite"             => array( "slug" => "section", "with_front" => true ),
        "query_var"           => true,
        "menu_position"       => 5,
        "menu_icon"           => "dashicons-media-document",
        "supports"            => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "section", $args );

}

add_action( 'init', 'lamour_register_my_cpts_section' );