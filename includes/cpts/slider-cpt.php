<?php
/// Register Custom Post Type
function lamour_slider_cpt() {

    $labels = array(
        'name'                  => _x( 'Slides', 'Post Type General Name', 'lamour' ),
        'singular_name'         => _x( 'Slide', 'Post Type Singular Name', 'lamour' ),
        'menu_name'             => __( 'Slider', 'lamour' ),
        'name_admin_bar'        => __( 'Slider', 'lamour' ),
        'archives'              => __( 'Slider', 'lamour' ),
        'attributes'            => __( 'Slide Attributes', 'lamour' ),
        'parent_item_colon'     => __( 'Parent slide:', 'lamour' ),
        'all_items'             => __( 'All Slide', 'lamour' ),
        'add_new_item'          => __( 'Add New Slide', 'lamour' ),
        'add_new'               => __( 'Add New', 'lamour' ),
        'new_item'              => __( 'New Slide', 'lamour' ),
        'edit_item'             => __( 'Edit Slide', 'lamour' ),
        'update_item'           => __( 'Update Slide', 'lamour' ),
        'view_item'             => __( 'View Slide', 'lamour' ),
        'view_items'            => __( 'View Slide', 'lamour' ),
        'search_items'          => __( 'Search Slide', 'lamour' ),
        'not_found'             => __( 'Not found', 'lamour' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'lamour' ),
        'featured_image'        => __( 'Featured Image', 'lamour' ),
        'set_featured_image'    => __( 'Set featured image', 'lamour' ),
        'remove_featured_image' => __( 'Remove featured image', 'lamour' ),
        'use_featured_image'    => __( 'Use as featured image', 'lamour' ),
        'insert_into_item'      => __( 'Insert into item', 'lamour' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'lamour' ),
        'items_list'            => __( 'Items list', 'lamour' ),
        'items_list_navigation' => __( 'Items list navigation', 'lamour' ),
        'filter_items_list'     => __( 'Filter items list', 'lamour' ),
    );
    $args = array(
        'label'                 => __( 'Slide', 'lamour' ),
        'description'           => __( 'Sliders For Lamour Homepage', 'lamour' ),
        'labels'                => $labels,
        'supports'              => array( 'title'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-image-flip-horizontal',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => false,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'show_in_rest'          => false,
    );
    register_post_type( 'lamour_slider', $args );

}
add_action( 'init', 'lamour_slider_cpt', 0 );