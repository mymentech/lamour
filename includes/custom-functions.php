<?php
function get_the_footer_logo()
{
    $image_id = cs_get_option('footer_image');
    if ($image_id) {
        $attachment      = wp_get_attachment_image_src($image_id, 'full');
        $footer_logo_url = ($attachment[0]);
        return esc_url($footer_logo_url);
    }
    return '';
}

function get_the_header_logo()
{
    $image_id = cs_get_option('header_logo');
    if ($image_id) {
        $attachment      = wp_get_attachment_image_src($image_id, 'full');
        $header_logo_url = ($attachment[0]);
        return esc_url($header_logo_url);
    }
    return '';
}

function header_site_icon()
{
    $image_id = cs_get_option('header_site_icon');
    if ($image_id) {
        $attachment       = wp_get_attachment_image_src($image_id, 'full');
        $header_site_icon = ($attachment[0]);
        return esc_url($header_site_icon);
    }
    return '';
}

/**
 * @param $image_id = Attachment/Image ID
 * @return string  = URL if the attachment
 */

function get_image_url_from_id($image_id)
{
    if ($image_id) {
        $attachment       = wp_get_attachment_image_src($image_id, 'full');
        $attachment = ($attachment[0]);
        return esc_url($attachment);
    }
    return '';
}

function insert_google_analytics()
{
    $code = cs_get_option('ga_code');
    $tag  = <<<TAG
<script async src="https://www.googletagmanager.com/gtag/js?id={$code}"></script>
<script>
/* <![CDATA[ */
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', '{$code}');
    /* ]]> */
</script>
TAG;

    echo $tag;

}

add_action('wp_footer', 'insert_google_analytics');


function print_lamour_menu_items($theme_location)
{
    if (($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location])) {
        $menu       = get_term($locations[$theme_location], 'nav_menu');
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        foreach ($menu_items as $menu_item) {
            echo '<a href="' . $menu_item->url . '"><div class="nav">' . $menu_item->title . '</div></a>' . "\n";
        }
    }
}
function print_lamour_footer_menu_items($theme_location)
{
    if (($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location])) {
        $menu       = get_term($locations[$theme_location], 'nav_menu');
        $menu_items = wp_get_nav_menu_items($menu->term_id);
        foreach ($menu_items as $menu_item) {
            echo '<a href="' . $menu_item->url . '"><span class="key nav">' . $menu_item->title . '</span></a>' . "\n";
        }
    }
}

function lamour_get_wc_categories(){
	$taxonomy     = 'product_cat';
	$orderby      = 'name';
	$show_count   = 0;      // 1 for yes, 0 for no
	$pad_counts   = 0;      // 1 for yes, 0 for no
	$hierarchical = 0;      // 1 for yes, 0 for no
	$title        = '';
	$empty        = 1;

	$args = array(
		'taxonomy'     => $taxonomy,
		'orderby'      => $orderby,
		'show_count'   => $show_count,
		'pad_counts'   => $pad_counts,
		'hierarchical' => $hierarchical,
		'title_li'     => $title,
		'hide_empty'   => $empty
	);

	return get_categories( $args );
}