<?php
define( 'CS_ACTIVE_FRAMEWORK',   true  ); // default true
define( 'CS_ACTIVE_METABOX',     true ); // default true
define( 'CS_ACTIVE_TAXONOMY',    false ); // default true
define( 'CS_ACTIVE_SHORTCODE',   false ); // default true
define( 'CS_ACTIVE_CUSTOMIZE',   false ); // default true
define( 'CS_ACTIVE_LIGHT_THEME',  true  ); // default false

require_once(get_theme_file_path('/framework/cs-framework/cs-framework.php'));
include_once(get_theme_file_path('/framework/tgm/tgmpa.php'));
include_once(get_theme_file_path('/classes/Mymentech.php'));
include_once(get_theme_file_path('/includes/metaboxes/slide-metabox.php'));
include_once(get_theme_file_path('/includes/metaboxes/section.php'));
include_once(get_theme_file_path('/includes/metaboxes/section-promo.php'));
include_once(get_theme_file_path('/includes/metaboxes/section-personalize.php'));
include_once(get_theme_file_path('/includes/metaboxes/section-hiw.php'));
include_once(get_theme_file_path('/includes/metaboxes/section-gift.php'));
include_once(get_theme_file_path('/includes/metaboxes/section-subscription.php'));
include_once(get_theme_file_path('/includes/metaboxes/page.php'));
include_once(get_theme_file_path('/includes/theme-option/options-panel.php'));
include_once(get_theme_file_path('/includes/assets.php'));
include_once(get_theme_file_path('/includes/custom-functions.php'));
include_once(get_theme_file_path('/includes/cpts/slider-cpt.php'));
include_once(get_theme_file_path('/includes/cpts/section-cpt.php'));
include_once(get_theme_file_path('/includes/sidebars/sidebar-single-post.php'));

