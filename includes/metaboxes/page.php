<?php

function lamour_section_picker_metabox($metaboxes){
    $page_id = 0;
    if ( isset( $_REQUEST['post'] ) || isset( $_REQUEST['post_ID'] ) ) {
        $page_id = empty( $_REQUEST['post_ID'] ) ? $_REQUEST['post'] : $_REQUEST['post_ID'];
    }

    $current_page_template = get_post_meta( $page_id, '_wp_page_template', true );
    if ( ! in_array( $current_page_template,
        array(
            'page-templates/about-us.php',
            'page-templates/gift.php',
            'page-templates/home.php',
            'page-templates/how-it-works.php',
            'page-templates/subscribe.php',
            )
    )) {
        return $metaboxes;
    }

    $metaboxes[] = array(
        'id'        => 'lamour-page-sections',
        'title'     => __( 'Sections', 'lamour' ),
        'post_type' => 'page',
        'context'   => 'normal',
        'priority'  => 'default',
        'sections'  => array(
            array(
                'name'     => 'lamour-page-sections-section-one',
                'icon'   => 'fa fa-image',
                'fields' => array(
                    array(
                        'id'    => 'sections',
                        'title'   => __( 'Select sections', 'lamour' ),
                        'type'    => 'group',
                        'button_title'    => __('New Section','lamour'),
                        'accordion_title' => __('Add New Section','lamour'),
                        'fields'=>array(
                            array(
                                'id'=>'section',
                                'title'=>__('Select sections','lamour'),
                                'type'=>'select',
                                'options'=>'post',
                                'query_args'=>array(
                                    'post_type'=>'section',
                                    'posts_per_page'=>-1
                                )
                            )
                        )
                    ),
                )
            ),

        )
    );

    return $metaboxes;
}
add_filter('cs_metabox_options','lamour_section_picker_metabox');