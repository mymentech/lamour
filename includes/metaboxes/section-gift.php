<?php

function lamour_gift_section_metabox($metaboxes) {
    $section_id = 0;

    if (isset($_REQUEST['post']) || isset($_REQUEST['post_ID'])) {
        $section_id = empty($_REQUEST['post_ID']) ? $_REQUEST['post'] : $_REQUEST['post_ID'];
    }

    if ('section' != get_post_type($section_id)) {
        return $metaboxes;
    }

    $section_meta = get_post_meta($section_id, 'lamour-section-type', true);
    $section_type = $section_meta['type'];
    if ('gift' != $section_type) {
        return $metaboxes;
    }

    $metaboxes[] = array(
        'id'        => 'lamour-section-gift',
        'title'     => __('gift Section', 'lamour'),
        'post_type' => 'section',
        'context'   => 'normal',
        'priority'  => 'default',
        'sections'  => array(
            array(
                'name'   => 'lamour-gift-section-one',
                'title'  => __('gift Data', 'lamour'),
                'icon'   => 'fa fa-image',
                'fields' => array(
                    array(
                        'id'    => 'gift_bg_image',
                        'title' => __('gift Background Image', 'lamour'),
                        'type'  => 'image',
                        'add_title'=>'Add Image'
                    ),
                    array(
                        'id'    => 'button_title',
                        'title' => __('Button Title', 'lamour'),
                        'type'  => 'text',
                    ),
                    array(
                        'id'    => 'button_url',
                        'title' => __('Button URL', 'lamour'),
                        'type'  => 'text',
                    ),
                ),

            ),
        ),
    );

    return $metaboxes;
}

add_filter('cs_metabox_options', 'lamour_gift_section_metabox');