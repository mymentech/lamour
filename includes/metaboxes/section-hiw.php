<?php

function lamour_hiw_section_metabox($metaboxes) {
    $section_id = 0;

    if (isset($_REQUEST['post']) || isset($_REQUEST['post_ID'])) {
        $section_id = empty($_REQUEST['post_ID']) ? $_REQUEST['post'] : $_REQUEST['post_ID'];
    }

    if ('section' != get_post_type($section_id)) {
        return $metaboxes;
    }

    $section_meta = get_post_meta($section_id, 'lamour-section-type', true);
    $section_type = $section_meta['type'];
    if ('how_it_works' != $section_type) {
        return $metaboxes;
    }

    $metaboxes[] = array(
        'id'        => 'lamour-section-hiw',
        'title'     => __('Columns For How It Works', 'lamour'),
        'post_type' => 'section',
        'context'   => 'normal',
        'priority'  => 'default',
        'sections'  => array(
            array(
                'name'   => 'lamour-hiw-section-one',
                'title'  => __('Add Columns', 'lamour'),
                'icon'   => 'fa fa-image',
                'fields' => array(
                    array(
                        'id'              => 'hiw_column_group',
                        'title'           => "How It works Columns",
                        'type'            => 'group',
                        'button_title'    => 'Add New Column',
                        'accordion_title' => 'Add Column',
                        'fields'          => array(
                            array(
                                'id'    => 'column_title',
                                'title' => __('Column Title', 'lamour'),
                                'type'  => 'Text',
                            ),
                            array(
                                'id'    => 'column_descriptions',
                                'title' => __('Column Description Title', 'lamour'),
                                'type'  => 'textarea',
                            ),
                        ),
                    ),
                ),

            ),
        ),
    );

    return $metaboxes;
}

add_filter('cs_metabox_options', 'lamour_hiw_section_metabox');