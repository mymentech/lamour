<?php

function lamour_personalize_section_metabox($metaboxes){
    $section_id = 0;

    if ( isset( $_REQUEST['post'] ) || isset( $_REQUEST['post_ID'] ) ) {
        $section_id = empty( $_REQUEST['post_ID'] ) ? $_REQUEST['post'] : $_REQUEST['post_ID'];
    }

    if('section'!=get_post_type($section_id)){
        return $metaboxes;
    }

    $section_meta = get_post_meta($section_id,'lamour-section-type',true);
    $section_type = $section_meta['type'];
    if('personalize'!=$section_type){
        return $metaboxes;
    }

    $metaboxes[] = array(
        'id'        => 'lamour-section-personalize',
        'title'     => __( 'personalize Settings', 'lamour' ),
        'post_type' => 'section',
        'context'   => 'normal',
        'priority'  => 'default',
        'sections'  => array(
            array(
                'name'     => 'lamour-personalize-section-one',
                'title'     => __( 'First Column', 'lamour' ),
                'icon'   => 'fa fa-image',
                'fields' => array(
                    array(
                        'id'    => 'personalize_image1',
                        'title'   => __( 'personalize-1 Image', 'lamour' ),
                        'type'    => 'image',
                    ),
                    array(
                        'id'    => 'personalize_title1',
                        'title'   => __( 'personalize-1 Title', 'lamour' ),
                        'type'    => 'text',
                    )
                )
            ),
            array(
                'name'     => 'lamour-personalize-section-two',
                'title'     => __( 'Second Column', 'lamour' ),
                'icon'   => 'fa fa-image',
                'fields' => array(
                    array(
                        'id'    => 'personalize_image2',
                        'title'   => __( 'personalize-2 Image', 'lamour' ),
                        'type'    => 'image',
                    ),
                    array(
                        'id'    => 'personalize_title2',
                        'title'   => __( 'personalize-2 Title', 'lamour' ),
                        'type'    => 'text',
                    )
                )
            ),

            array(
                'name'     => 'lamour-personalize-section-three',
                'title'     => __( 'Third Column', 'lamour' ),
                'icon'   => 'fa fa-image',
                'fields' => array(
                    array(
                        'id'    => 'personalize_image3',
                        'title'   => __( 'personalize-3 Image', 'lamour' ),
                        'type'    => 'image',
                    ),
                    array(
                        'id'    => 'personalize_title3',
                        'title'   => __( 'personalize-3 Title', 'lamour' ),
                        'type'    => 'text',
                    )
                )
            ),

        )
    );

    return $metaboxes;
}
add_filter('cs_metabox_options','lamour_personalize_section_metabox');