<?php

function lamour_promo_section_metabox($metaboxes){
    $section_id = 0;

    if ( isset( $_REQUEST['post'] ) || isset( $_REQUEST['post_ID'] ) ) {
        $section_id = empty( $_REQUEST['post_ID'] ) ? $_REQUEST['post'] : $_REQUEST['post_ID'];
    }

    if('section'!=get_post_type($section_id)){
        return $metaboxes;
    }

    $section_meta = get_post_meta($section_id,'lamour-section-type',true);
    $section_type = $section_meta['type'];
    if('promo'!=$section_type){
        return $metaboxes;
    }

    $metaboxes[] = array(
        'id'        => 'lamour-section-promo',
        'title'     => __( 'Promo Settings', 'lamour' ),
        'post_type' => 'section',
        'context'   => 'normal',
        'priority'  => 'default',
        'sections'  => array(
            array(
                'name'     => 'lamour-promo-section-one',
                'title'     => __( 'First Column', 'lamour' ),
                'icon'   => 'fa fa-image',
                'fields' => array(
                    array(
                        'id'    => 'promo_image1',
                        'title'   => __( 'Promo-1 Image', 'lamour' ),
                        'type'    => 'image',
                    ),
                    array(
                        'id'    => 'promo_title1',
                        'title'   => __( 'Promo-1 Title', 'lamour' ),
                        'type'    => 'text',
                    ),array(
                        'id'    => 'promo_description1',
                        'title'   => __( 'Promo-1 Description', 'lamour' ),
                        'type'    => 'textarea',
                    ),
                )
            ),
            array(
                'name'     => 'lamour-promo-section-two',
                'title'     => __( 'Second Column', 'lamour' ),
                'icon'   => 'fa fa-image',
                'fields' => array(
                    array(
                        'id'    => 'promo_image2',
                        'title'   => __( 'Promo-2 Image', 'lamour' ),
                        'type'    => 'image',
                    ),
                    array(
                        'id'    => 'promo_title2',
                        'title'   => __( 'Promo-2 Title', 'lamour' ),
                        'type'    => 'text',
                    ),array(
                        'id'    => 'promo_description2',
                        'title'   => __( 'Promo-2 Description', 'lamour' ),
                        'type'    => 'textarea',
                    ),
                )
            ),

            array(
                'name'     => 'lamour-promo-section-three',
                'title'     => __( 'Third Column', 'lamour' ),
                'icon'   => 'fa fa-image',
                'fields' => array(
                    array(
                        'id'    => 'promo_image3',
                        'title'   => __( 'Promo-3 Image', 'lamour' ),
                        'type'    => 'image',
                    ),
                    array(
                        'id'    => 'promo_title3',
                        'title'   => __( 'Promo-3 Title', 'lamour' ),
                        'type'    => 'text',
                    ),array(
                        'id'    => 'promo_description3',
                        'title'   => __( 'Promo-3 Description', 'lamour' ),
                        'type'    => 'textarea',
                    ),
                )
            ),

        )
    );

    return $metaboxes;
}
add_filter('cs_metabox_options','lamour_promo_section_metabox');