<?php

add_filter('cs_metabox_options', 'lamour_section_type_metabox');

function lamour_section_type_metabox($metaboxes) {
    $metaboxes[] = array(
        'id'        => 'lamour-section-type',
        'title'     => __('section type', 'lamour'),
        'post_type' => 'section',
        'context'   => 'normal',
        'priority'  => 'default',
        'sections'  => array(
            array(
                'name'   => 'lamour-section-type-section-one',
                'icon'   => 'fa fa-image',
                'fields' => array(
                    array(
                        'id'    => 'type',
                        'title' => __('Select section type', 'lamour'),
                        'type'  => 'select',

                        'options' => array(
                            'about_lamour'    => __('About Lamour', 'lamour'),
                            'email_subscribe' => __('Email Subscribe', 'lamour'),
                            'gift'            => __('Gift', 'lamour'),
                            'how_it_works'    => __('How It works', 'lamour'),
                            'plain_text'     => __('Plain Text', 'lamour'),
                            'make_happy'      => __('Make a Friend Happy', 'lamour'),
                            'personalize'     => __('Personalize', 'lamour'),
                            'promo'     => __('Promo Section', 'lamour'),
                            'subscription'    => __('Subscription', 'lamour'),
                            'what_in_the_box' => __('What in the Box', 'lamour'),
                        )
                    ),
                )
            ),

        )
    );

    return $metaboxes;
}

