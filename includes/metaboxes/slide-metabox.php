<?php
/**
 * Created by PhpStorm.
 * User: mozammel
 * Date: 9/3/18
 * Time: 9:46 PM
 */


function cs_slide_metabox_options($options)
{
    $options[] = array(
        'id' => 'slide_metabox',
        'title' => __('Slide Info', 'lamour'),
        'post_type' => 'lamour_slider', // or post or CPT or array( 'page', 'post' )
        'context' => 'normal',
        'priority' => 'default',
        'sections' => array(

            // begin section
            array(
                'name' => 'slide_data',
                'title' => __('Slide Data', 'lamour'),
                'icon' => 'fa fa-image',
                'fields' => array(

                    array(
                        'id' => 'slide_title',
                        'type' => 'text',
                        'title' => __('Slide Title', 'lamour'),
                    ),
                    array(
                        'id'        => 'slide_image',
                        'type'      => 'image',
                        'title'     => __('Slide Image', 'lamour'),
                        'add_title' => 'Select Image',
                    ),
                    array(
                        'id' => 'slide_description',
                        'type' => 'textarea',
                        'title' => __('Slide Description', 'lamour'),
                    ),
                    array(
                        'id' => 'slide_button_text',
                        'type' => 'text',
                        'title' => __('Slide Button\'s Text', 'lamour'),
                        'attributes' => array(
                            'placeholder' => 'Button',
                        ),
                    ),
                    array(
                        'id' => 'slide_button_url',
                        'type' => 'text',
                        'title' => __('Slide Button\'s Url', 'lamour'),
                        'attributes' => array(
                            'placeholder' => 'http://',
                        ),
                    )
                ),
            ),
        ),
    );


    return $options;
}

add_filter('cs_metabox_options', 'cs_slide_metabox_options');