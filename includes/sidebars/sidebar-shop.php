<?php

register_sidebar(
    array(
        'name'=>'Footer Right',
        'id'=>'footer-3',
        'description'=>__('Footer First Column Widget','wolfwireless'),
        'before_widget'=>'<div id="%1$s" class="widget %2$s">',
        'after_widget'=>'</div>',
        'before_title'=>'<h5 class="widget-title">',
        'after_title'=>'</h5>'
    )
);