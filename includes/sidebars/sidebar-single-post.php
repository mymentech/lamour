<?php


register_sidebar(
    array(
        'name'=>'Single Post Right',
        'id'=>'single-post-right',
        'description'=>__('Single Post Right Widget','wolfwireless'),
        'before_widget'=>'<div id="%1$s" class="widget content-small %2$s">',
        'after_widget'=>'</div>',
        'before_title'=>'<h5 class="widget-title">',
        'after_title'=>'</h5>'
    )
);