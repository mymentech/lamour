<?php
if (!defined('ABSPATH')) {
    die;
} // Cannot access pages

function lamour_theme_option_init()
{
    $settings = array(
        'menu_title' => __('Lamour Options', 'lamour'),
        'menu_type' => 'menu',
        'menu_slug' => 'lamour-options-panel',
        'framework_title' => __('Lamour Options', 'lamour'),
        'ajax_save' => true,
        'show_reset_all' => true,
        'menu_icon'=> 'dashicons-wordpress',
        'menu_position'=>6,
    );


    CSFramework::instance($settings, array());
}

add_action('init', 'lamour_theme_option_init');


function lamour_theme_options($options)
{
    $options[] = array(
        'name' => 'header_options',
        'title' => __('Header Options','lamour'),
        'icon' => 'dashicons dashicons-arrow-up-alt',
        'fields' => array(

            // a text field
            array(
                'id' => 'header_site_icon',
                'type'      => 'image',
                'title'     => __('Site Icon (Favicon)', 'lamour'),
                'add_title' => 'Add Site Icon',
            ),
            array(
                'id' => 'header_logo',
                'type'      => 'image',
                'title'     => __('Header Logo', 'lamour'),
                'add_title' => 'Add Logo',
            ),

        )
    );
    $options[] = array(
        'name' => 'mailchim_api',
        'title' => __('MailChimp API','lamour'),
        'icon' => 'dashicons dashicons-email-alt',
        'fields' => array(

            // a text field
            array(
                'id' => 'mailchimp_api_key',
                'type'      => 'text',
                'title'     => __('MailChim API key', 'lamour'),
            ),
            array(
                'id' => 'mailchimp_list_id',
                'type'      => 'text',
                'title'     => __('MailChimp List ID', 'lamour'),
            ),

        )
    );
    $options[] = array(
        'name' => 'social_profiles',
        'title' => __('Social Profiles','lamour'),
        'icon' => 'dashicons dashicons-groups',
        'fields' => array(

            // a text field
            array(
                'id' => 'lamour_facebook',
                'type'      => 'text',
                'title'     => __('Facebook Profile URL', 'lamour'),
            ),
            array(
                'id' => 'lamour_twitter',
                'type'      => 'text',
                'title'     => __('Twitter Profile URL', 'lamour'),
            ),
            array(
                'id' => 'lamour_gplus',
                'type'      => 'text',
                'title'     => __('Google Plus URL', 'lamour'),

            ),
            array(
                'id' => 'lamour_email',
                'type'      => 'text',
                'title'     => __('E-mail Address', 'lamour'),

            ),


        )
    );
    $options[] = array(
        'name' => 'footer_options',
        'title' => __('Footer Options', 'lamour'),
        'icon' => 'dashicons dashicons-arrow-down-alt',
        'fields' => array(

            array(
                'id'        => 'footer_image',
                'type'      => 'image',
                'title'     => __('Footer Image Icon', 'lamour'),
                'add_title' => 'Add Footer Icon',
            ),

            // a textarea field
            array(
                'id' => 'copyright_text',
                'type' => 'text',
                'title' => __('Footer Copyright Text', 'lamour'),
                'default'=>__('LAMOUR SECRET © 2018 ALL RIGHTS RESERVED','lamour'),
            ),
            array(
                'id' => 'ga_code',
                'type' => 'text',
                'title' => __('Google Analytics Code', 'lamour'),
                'attributes'=>array(
                    'placeholder'=>'Eg: UA-XXXXXXX-X'
                ),
            ),

        )
    );


    return $options;

}

add_filter('cs_framework_options', 'lamour_theme_options');