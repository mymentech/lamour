<?php
/**
Template Name: About Us
 */
get_header();

$lamour_current_page_id = get_the_ID();
$lamour_page_meta       = get_post_meta($lamour_current_page_id, 'lamour-page-sections', true);
foreach ($lamour_page_meta['sections'] as $lamour_page_section):
    $lamour_section_id   = $lamour_page_section['section'];
    $lamour_section_meta = get_post_meta($lamour_section_id, 'lamour-section-type', true);
    $lamour_section_type = $lamour_section_meta['type'];
    get_template_part("sections/{$lamour_section_type}");

endforeach;
get_footer() ?>