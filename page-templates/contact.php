<?php
/**
Template Name: Contat Us
 */
get_header(); ?>

<div class="section padding whiteBG" id="contact">
  <div class="container small textCenter">
    <div class="title-script">Contact Us</div>
    <div class="clear"></div>
    <span class="text alt"></span>
    <div class="clear"></div>

    <div id="account-details">
    <div class="container small">
        <div class="fs-form fs-form-full" id="myform">
            <?php


            if(have_posts()){

                while ( have_posts() ) : the_post();

                    the_content();

                endwhile;

            }
            
            ?>


        </div>
    <!--<form id="myform" class="fs-form fs-form-full" autocomplete="off" action="/actions/mailer" method="post">
    <input type="hidden" name="return" value="http://lamoursecret.com/contact" />
      <ol class="fs-fields">
        <li>
          <label class="fs-field-label fs-anim-upper" for="q1">First Name</label>
          <input class="fs-anim-lower" id="q1" name="fname" type="text" required/>
          <div class="clear2"></div>
          <label class="fs-field-label fs-anim-upper" for="q3">Last Name</label>
          <input class="fs-anim-lower" id="q3" name="lname" type="text" required="required"/>
          <div class="clear2"></div>
          <label class="fs-field-label fs-anim-upper" for="q12">Email Address</label>
          <input class="fs-anim-lower" id="q12" name="email" type="email" required="required"/>
          <div class="clear2"></div>
          <label class="fs-field-label fs-anim-upper" for="q7">Message</label>
          <textarea class="fs-anim-lower" id="q7" rows="10" name="message" required="required" ></textarea>
        </li>
      </ol>

      <div class="textCenter">
      <input class="btn blackBG white" type="submit" value="Send Message" />
      </div>

    </form><!-- /fs-form -->


    </div>
    </div>

  </div>
</div>

<?php

get_footer();

?>