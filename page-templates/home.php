<?php
/**
 * Template Name: HomePage
 **/




get_header() ?>

<?php
$lamour_current_page_id = get_the_ID();
$lamour_page_meta       = get_post_meta($lamour_current_page_id, 'lamour-page-sections', true);

?>

    <div id="header-slider">
        <?php

        $args = array(
            'post_type'   => 'lamour_slider',
            'post_status' => 'publish',
        );

        $slider = new WP_Query($args);

        while ($slider->have_posts()) : $slider->the_post();

            $slide_meta = get_post_meta(get_the_ID(), 'slide_metabox', true);
            $slide_meta = (object)$slide_meta;
            $image_url  = get_image_url_from_id($slide_meta->slide_image);

            ?>

            <!-- Start single item -->
            <div class="mySlides">
                <div class="section blackBG bgImg noPadding" id="homeBox">
                    <div class="overlayImg padding" style="background-image:url('<?php echo $image_url ?>');"></div>
                    <div class="fullSection">
                        <div class="fullSectionContent">
                            <div class="container">
                                <div class="holder white">
                                    <span class="text-script"><?php if (isset($slide_meta->slide_title)) echo $slide_meta->slide_title ?></span>
                                    <div class="clear"></div>
                                    <span class="text"><?php if (isset($slide_meta->slide_description)) echo $slide_meta->slide_description ?></span>
                                    <div class="clear2"></div>
                                    <?php if(!is_user_logged_in()): ?>
                                    <a href="#account-modal" class="modal-pop">
                                        <div class="button red whiteBG slider_button">
                                            <b><?php if (isset($slide_meta->slide_button_text)) echo $slide_meta->slide_button_text ?></b>
                                        </div>
                                    </a>
                                <?php else: ?>
                                <a href="<?php if (isset($slide_meta->slide_button_url)) echo $slide_meta->slide_button_url ?>">
                                    <div class="button red whiteBG slider_button">
                                        <b><?php if (isset($slide_meta->slide_button_text)) echo $slide_meta->slide_button_text ?></b>
                                    </div>
                                </a>

                                <?php endif ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php endwhile; ?>

        <!-- end single item -->

        <button class="  w3-display-left" onclick="plusDivs(-1)"><i class="fa fa-4x fa-chevron-circle-left"></i></button>
        <button class="  w3-display-right" onclick="plusDivs(1)"><i class="fa fa-4x fa-chevron-circle-right"></i></button>

    </div>
    <!-- slider end -->
<?php
foreach ($lamour_page_meta['sections'] as $lamour_page_section):
    $lamour_section_id   = $lamour_page_section['section'];
    $lamour_section_meta = get_post_meta($lamour_section_id, 'lamour-section-type', true);
    $lamour_section_type = $lamour_section_meta['type'];
    get_template_part("sections/{$lamour_section_type}");

endforeach;
?>


<?php get_footer(); ?>