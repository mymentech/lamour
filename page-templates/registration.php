<?php
/**
 * Template Name: Registration Page
 */
get_header('registration');
$lrn = wp_create_nonce('lamour_registration')
?>
<div class="container fullHeightMin full" id="ls-registration">
    <div class="fs-form-wrap fullHeightMin" id="fs-form-wrap">
        <form id="myform" class="fs-form fs-form-full" autocomplete="off" action="<?php echo admin_url('admin-post.php') ?>" method="post">
            <div class="subtitle grey2"><?php _e("Registration") ?></div>
            <ol class="fs-fields">
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q1"><?php _e("What is your firstname?","lamour") ?></label>
                    <input class="fs-anim-lower" id="q1" name="firstname" type="text" placeholder="John" required/>
                    <div class="clear2"></div>
                    <label class="fs-field-label fs-anim-upper" for="q2"><?php _e("...your lastname?","lamour") ?></label>
                    <input class="fs-anim-lower" id="q2" name="lastname" type="text" placeholder="Doe" required/>
                    <div class="clear2"></div>
                    <label class="fs-field-label fs-anim-upper" for="q3"><?php _e("Enter your age","lamour") ?></label>
                    <input class="fs-anim-lower" id="q3" name="age" type="text"/>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q4"><?php _e("What is your gender","lamour") ?></label>
                    <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
                        <span><input id="q4b" name="gender" type="radio" value="male"/><label for="q4b" class="male-icon r"><?php _e("Male","lamour") ?></label></span>
                        <span><input id="q4c" name="gender" type="radio" value="female"/><label for="q4c" class="female-icon r"><?php _e("Female","lamour") ?></label></span>
                    </div>
                    <label class="fs-field-label fs-anim-upper" for="q5"><?php _e("What is your Sexual Preference?","lamour") ?></label>
                    <div class="fs-radio-group fs-radio-custom clearfix fs-anim-lower">
                        <span><input id="q5b" name="preference" type="radio" value="male"/><label for="q5b" class="male-icon b"><?php _e("Male","lamour") ?></label></span>
                        <span><input id="q5c" name="preference" type="radio" value="female"/><label for="q5c" class="female-icon b"><?php _e("Female","lamour") ?></label></span>
                        <span><input id="q5d" name="preference" type="radio" value="both"/><label for="q5d" class="both-icon b"><?php _e("Both","lamour") ?></label></span>
                    </div>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper"><?php _e("How would you describe your sex life?","lamour") ?></label>
                    <select name="sexlife" class='chzn-select' id='sexlife'>
                        <option value="Simple - you like it just how you like it" selected="selected"><?php _e("Simple - you like it just how you like it","lamour") ?></option>
                        <option value="Adventurous - you are looking to explore"><?php _e("Adventurous - you are looking to explore","lamour") ?></option>
                        <option value="Wild - you have explored and know how to turn it up"><?php _e("Wild - you have explored and know how to turn it up","lamour") ?></option>
                    </select>
                </li>

                <li>
                    <label class="fs-field-label fs-anim-upper"><?php _e("Select all that interest you below:","lamour") ?></label>
                    <select name="interest[]" class='chzn-select' id='interest' multiple="multiple">
                        <option value="Vaginal"><?php _e("Vaginal","lamour") ?></option>
                        <option value="Anal"><?php _e("Anal","lamour") ?></option>
                        <option value="Oral"><?php _e("Oral","lamour") ?></option>
                        <option value="Masturbation"><?php _e("Masturbation","lamour") ?></option>
                        <option value="Bondage"><?php _e("Bondage","lamour") ?></option>
                        <option value="Enhancement Pills for Him"><?php _e("Enhancement Pills for Him","lamour") ?></option>
                        <option value="Enhancement Pills for Her"><?php _e("Enhancement Pills for Her","lamour") ?></option>
                        <option value="Penis Extensions"><?php _e("Penis Extensions","lamour") ?></option>
                        <option value="Sex in the Water"><?php _e("Sex in the Water","lamour") ?></option>
                        <option value="Kegal balls"><?php _e("Kegal balls","lamour") ?></option>
                        <option value="Different positions"><?php _e("Different positions","lamour") ?></option>
                        <option value="Nipple stimulation"><?php _e("Nipple stimulation","lamour") ?></option>
                        <option value="G spot Stimulation"><?php _e("G spot Stimulation","lamour") ?></option>
                        <option value="Testicular Stimulation"><?php _e("Testicular Stimulation","lamour") ?></option>
                        <option value="Anal Stimulation"><?php _e("Anal Stimulation","lamour") ?></option>
                        <option value="Dildos"><?php _e("Dildos","lamour") ?></option>
                        <option value="Cock Rings"><?php _e("Cock Rings","lamour") ?></option>
                        <option value="Vibrators"><?php _e("Vibrators","lamour") ?></option>
                        <option value="Spankings"><?php _e("Spankings","lamour") ?></option>
                        <option value="Rough Sex"><?php _e("Rough Sex","lamour") ?></option>
                        <option value="Submission ( Taking Orders )"><?php _e("Submission ( Taking Orders )","lamour") ?></option>
                        <option value="Dominating ( Giving Orders )"><?php _e("Dominating ( Giving Orders )","lamour") ?></option>
                        <option value="Top (Giver)"><?php _e("Both","Top (Giver)") ?></option>
                        <option value="Bottom ( Receiver )"><?php _e("Bottom ( Receiver )","lamour") ?></option>
                        <option value="Versatile ( Giver/Receiver )"><?php _e("Versatile ( Giver/Receiver )","lamour") ?></option>
                    </select>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper"><?php _e("Select all that you are DEFINITELY NOT interested in:","lamour") ?></label>
                    <select name="not_interest[]" class='chzn-select' id='not_interest' multiple="multiple">
                        <option value="Vaginal"><?php _e("Vaginal","lamour") ?></option>
                        <option value="Anal"><?php _e("Anal","lamour") ?></option>
                        <option value="Oral"><?php _e("Oral","lamour") ?></option>
                        <option value="Masturbation"><?php _e("Masturbation","lamour") ?></option>
                        <option value="Bondage"><?php _e("Bondage","lamour") ?></option>
                        <option value="Enhancement Pills for Him"><?php _e("Enhancement Pills for Him","lamour") ?></option>
                        <option value="Enhancement Pills for Her"><?php _e("Enhancement Pills for Her","lamour") ?></option>
                        <option value="Penis Extensions"><?php _e("Penis Extensions","lamour") ?></option>
                        <option value="Sex in the Water"><?php _e("Sex in the Water","lamour") ?></option>
                        <option value="Kegal balls"><?php _e("Kegal balls","lamour") ?></option>
                        <option value="Different positions"><?php _e("Different positions","lamour") ?></option>
                        <option value="Nipple stimulation"><?php _e("Nipple stimulation","lamour") ?></option>
                        <option value="G spot Stimulation"><?php _e("G spot Stimulation","lamour") ?></option>
                        <option value="Testicular Stimulation"><?php _e("Testicular Stimulation","lamour") ?></option>
                        <option value="Anal Stimulation"><?php _e("Anal Stimulation","lamour") ?></option>
                        <option value="Dildos"><?php _e("Dildos","lamour") ?></option>
                        <option value="Cock Rings"><?php _e("Cock Rings","lamour") ?></option>
                        <option value="Vibrators"><?php _e("Vibrators","lamour") ?></option>
                        <option value="Spankings"><?php _e("Spankings","lamour") ?></option>
                        <option value="Rough Sex"><?php _e("Rough Sex","lamour") ?></option>
                        <option value="Submission ( Taking Orders )"><?php _e("Submission ( Taking Orders )","lamour") ?></option>
                        <option value="Dominating ( Giving Orders )"><?php _e("Dominating ( Giving Orders )","lamour") ?></option>
                        <option value="Top (Giver)"><?php _e("Both","Top (Giver)") ?></option>
                        <option value="Bottom ( Receiver )"><?php _e("Bottom ( Receiver )","lamour") ?></option>
                        <option value="Versatile ( Giver/Receiver )"><?php _e("Versatile ( Giver/Receiver )","lamour") ?></option>
                    </select>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q12" data-info="We won't send you spam, we promise..."><?php _e("What's your email address?","lamour") ?>
                    </label>
                    <input class="fs-anim-lower" id="q12" name="email" type="email" placeholder="name@domain.com" required/>
                    <div class="clear2"></div>
                    <label class="fs-field-label fs-anim-upper" for="q10"><?php _e("Create your password","lamour") ?></label>
                    <input class="fs-anim-lower" id="q10" name="password" type="password" required/>
                    <div class="clear2"></div>
                    <label class="fs-field-label fs-anim-upper" for="q11"><?php _e("Confirm your password","lamour") ?></label>
                    <input class="fs-anim-lower" id="q11" name="confirm" type="password" required/>
                </li>
                <li>
                    <label class="fs-field-label fs-anim-upper" for="q1"><?php _e("How did you hear about us?","lamour") ?></label>
                    <input class="fs-anim-lower" id="q1" name="reference" type="text" placeholder="Facebook, Friend, Email, Ad, etc..." required/>
                </li>

            </ol><!-- /fs-fields -->
            <div class="container small text alt">
	            <?php _e("By clicking \"Complete Registration\", you are agreeing to our Terms and Conditions","lamour") ?>
            </div>
            <div class="clear2"></div>
            <?php wp_nonce_field('lamour_registration','lamour_s') ?>
            <input type="hidden" value="lamour_registration" name="action">
            <button class="fs-submit" type="submit"><?php _e("Complete Registration","lamour") ?></button>
            <div class="clear5"></div>
        </form><!-- /fs-form -->
    </div><!-- /fs-form-wrap -->

</div><!-- /container -->

<div class="clear3"></div>
<div class="clear3"></div>
<?php get_footer() ?>