<?php
/*
Template Name: Blog Template
*/

get_header();
?>



<div class="section padding whiteBG">
	<div class="container textCenter">
		<div class="title-script"><?php echo get_the_title(); ?></div>

		<div class="clear5"></div>
		<div class="clear3"></div>
		
					
			
			<div class="promo-section textLeft">


                <?php

                $args = array(
                    'post_type'   => 'post',
                    'post_status' => 'publish',
                );

                $lsblog = new WP_Query( $args );


                $ccheck =0;
                while ($lsblog->have_posts()) : $lsblog->the_post();



                ?>



				<div class="trip floatL" style="margin:0 10px 16px !important;">
					<a href="<?php echo get_the_permalink(); ?>"><div class="blog-thumb bgImg" style="background-image:url(<?php echo esc_url(get_the_post_thumbnail_url()); ?>);"></div></a>
					<div class="clear"></div>
					<a href="<?php echo get_the_permalink(); ?>"><div class="subtitle red"><?php echo get_the_title(); ?></div></a>
					<div class="clear0"></div>
					<span class="text alt"><?php echo wp_trim_words( get_the_content(), 15, ' <a href="'.get_the_permalink().'"> ...</a>' ); ?>    </span>
				</div>

                <?php
                    $ccheck++;

                    if(3==$ccheck){

                    echo "<div class='clear'></div>";

                    $ccheck=0;

                    } ?>



                <?php endwhile; ?>




            </div>
        <!-- end promo-section -->
	
	</div>
    <!-- end container -->
</div>

<!-- end section -->

</div>



<?php get_footer(); ?>