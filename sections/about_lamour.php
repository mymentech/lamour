<?php
global $lamour_section_id;

$lamour_about         = get_post($lamour_section_id);
$lamour_about_title   = $lamour_about->post_title;
$lamour_about_content = $lamour_about->post_content;
$lamour_bg_image      = get_the_post_thumbnail_url($lamour_section_id, 'full');
if(!$lamour_bg_image){
    $lamour_bg_image = get_theme_file_uri('/assets/images/about-bg.jpg');
}
?>

<div class="section noPadding whiteBG page-header">
    <div class="overlayImg grayscale" style="background-image:url('<?php echo esc_url($lamour_bg_image)?>');"></div>
    <div class="overlay"></div>
    <div class="fullSection"><div class="fullSectionContent">
            <div class="container small textCenter">
                <span class="title-script red">
                    <?php echo esc_html($lamour_about_title) ?>
                </span>
                <div class="clear3"></div>
                <span class="text white">
                    <?php echo apply_filters('the-title',$lamour_about_content) ?>
                </span>
            </div>
        </div></div>
</div>

