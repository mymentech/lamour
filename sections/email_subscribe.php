<?php
global $lamour_section_id;

$lamour_section_email             = get_post( $lamour_section_id );
$lamour_section_email_title       = $lamour_section_email->post_title;
$lamour_section_email_description = $lamour_section_email->post_content;

$lamour_bg_image = get_the_post_thumbnail_url($lamour_section_id,'full');
$nonce = wp_create_nonce('lamour_mailchimp_email');

?>
<div class="section padding lightBG bgImg subscribe-section" style="background-image:url('<?php echo esc_url($lamour_bg_image)?>');">
    <div class="fullSection">
        <div class="fullSectionContent">
            <div class="container textCenter">
                <div class="title-script red"><?php echo esc_html($lamour_section_email_title) ?></div>
            </div>
        </div>
    </div>

    <form class="subscribeForm" name="subscribe-form" method="post" action="<?php site_url('/') ?>">
        <input type="hidden" name="return" value="http://lamoursecret.com/">
        <?php wp_nonce_field('lamour_mailchimp_email') ?>
        <input type="text" name="email" id="email" placeholder="<?php echo esc_attr($lamour_section_email_description) ?>" class="subInput" required="required" autocomplete="off">
        <input type="submit" class="cool-btn button white" id="subButton" value="Sign Up">
    </form>
</div>
