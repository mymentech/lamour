<?php
global $lamour_section_id;
$lamour_gift_meta = get_post_meta( $lamour_section_id, 'lamour-section-gift', true );

$lamour_gift_bg = get_template_directory_uri() . '/assets/images/bg_ornament.jpg';

if(isset($lamour_gift_meta['gift_bg_image'])){
    $lamour_gift_bg = wp_get_attachment_image_url($lamour_gift_meta['gift_bg_image'],'full');
}

$lamour_gift_image = get_the_post_thumbnail_url($lamour_section_id,'full');



$lamour_gift            = get_post($lamour_section_id );
$lamour_gift_title       = $lamour_gift->post_title;
$lamour_gift_content = $lamour_gift->post_content;


?>

<div class="section whiteBG bgImg subscribe-header" style="background-image:url('<?php echo esc_url($lamour_gift_bg)?>');">
    <div class="fullSection"><div class="fullSectionContent">
            <div class="container textLeft">
                <div class="floatL half small">
                    <img src="<?php echo esc_url($lamour_gift_image) ?>" />
                </div>

                <div class="half large textLeft floatL">
                    <span class="text-script white">
                        <?php echo esc_html($lamour_gift_title) ?>
                    </span>
                    <div class="clear"></div>
                    <span class="text white">
                        <?php echo apply_filters('the_content',$lamour_gift_content) ?>
                    </span>
                    <div class="clear2"></div>
                    <a href="<?php if(isset($lamour_gift_meta['button_url'])) echo esc_url($lamour_gift_meta['button_url'])?>">
                        <div class="button primary"><?php if(isset($lamour_gift_meta['button_title'])) echo esc_html($lamour_gift_meta['button_title'])?></div>
                    </a>
                </div>
            </div>
        </div></div>
</div>


