<?php
global $lamour_section_id;
$lamour_hiw_meta = get_post_meta($lamour_section_id, 'lamour-section-hiw', true);

$hiw_columns = $lamour_hiw_meta['hiw_column_group'];



$hiw_section = get_post($lamour_section_id);
$hiw_title   = $hiw_section->post_title;
$hiw_content = $hiw_section->post_content;
$hiw_column_num = 1;


?>

<div class="section padding whiteBG">
    <div class="container textCenter">
        <div class="title-script">
            <?php echo esc_html($hiw_title) ?>
        </div>
        <div class="clear5"></div>

        <div class="text textLeft">
            <?php echo wp_kses_post($hiw_content) ?>
        </div>

        <div class="clear5"></div>

        <div class="promo-section textLeft">

            <?php foreach ($hiw_columns as $num => $column): ?>
                <div class="trip">
                <span class="subtitle red">
                    <?php
                    if (isset($column['column_title'])) {
                        printf("%d. %s", $hiw_column_num, $column['column_title']);
                    }
                    ?>
                </span>
                    <div class="clear0"></div>
                    <span class="text alt">
                    <?php
                    if (isset($column['column_descriptions'])) {
                        echo esc_html($column['column_descriptions']);
                    }
                    $hiw_column_num++;
                    ?>
                </span>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="clear3"></div>


    </div>
</div>