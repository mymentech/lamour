<?php
global $lamour_section_id;

$lamour_happy         = get_post($lamour_section_id);
$lamour_happy_title   = $lamour_happy->post_title;
$lamour_happy_content = $lamour_happy->post_content;
$thumb_url            = get_the_post_thumbnail_url($lamour_section_id, 'full');

?>

<div class="section whiteBG padding">
    <div class="container textCenter text">
        <div class="title-script grey2"><?php echo esc_html($lamour_happy_title) ?></div>

        <div class="clear"></div>

        <div class="container small textCenter">
            <img src="<?php echo esc_url($thumb_url) ?>" id="giftImg"/>
        </div>

        <div class="clear2"></div>

        <div class="container small text">
            <?php echo apply_filters('the_content', $lamour_happy_content) ?>
        </div>

        <div class="clear5"></div>

    </div>
</div>
