<?php
global $lamour_section_id;
$lamour_personalize_meta = get_post_meta($lamour_section_id, 'lamour-section-personalize', true);

$lamour_personalize_image1 = get_template_directory_uri() . '/assets/images/personalize_triangle.png';
$lamour_personalize_image2 = get_template_directory_uri() . '/assets/images/personalize_cuffs.png';
$lamour_personalize_image3 = get_template_directory_uri() . '/assets/images/personalize_shoe.png';

if (isset($lamour_personalize_meta['personalize_image1'])) {
    $lamour_personalize_image1 = wp_get_attachment_image_src($lamour_personalize_meta['personalize_image1'], 'full')[0];
}
if (isset($lamour_personalize_meta['personalize_image2'])) {
    $lamour_personalize_image2 = wp_get_attachment_image_src($lamour_personalize_meta['personalize_image2'], 'full')[0];
}
if (isset($lamour_personalize_meta['personalize_image3'])) {
    $lamour_personalize_image3 = wp_get_attachment_image_src($lamour_personalize_meta['personalize_image3'], 'full')[0];
}


$lamour_personalize         = get_post($lamour_section_id);
$lamour_personalize_title   = $lamour_personalize->post_title;
$lamour_personalize_content = $lamour_personalize->post_content;


?>

<div class="section whiteBG border-top padding">
    <div class="container textCenter text">
        <div class="title-script grey2">
            <?php echo esc_html($lamour_personalize_title) ?>
        </div>

        <div class="clear5"></div>

        <div id="promo-subscribe-section">
            <div class="trip">
                <img src="<?php echo esc_url($lamour_personalize_image1) ?>"/>
                <div class="clear0"></div>
                <span class="subtitle">
                    <?php if(isset($lamour_personalize_meta['personalize_title1']))echo esc_html($lamour_personalize_meta['personalize_title1']) ?>
                </span>
                <div class="clear0"></div>
                <span class="text alt"></span>
            </div>

            <div class="trip">
                <img src="<?php echo esc_url($lamour_personalize_image2) ?>"/>
                <div class="clear0"></div>
                <span class="subtitle">
                    <?php if(isset($lamour_personalize_meta['personalize_title2']))echo esc_html($lamour_personalize_meta['personalize_title2']) ?>
                </span>
                <div class="clear0"></div>
                <span class="text alt"></span>
            </div>

            <div class="trip">
                <img src="<?php echo esc_url($lamour_personalize_image3) ?>"/>
                <div class="clear0"></div>
                <span class="subtitle">
                    <?php if(isset($lamour_personalize_meta['personalize_title3']))echo esc_html($lamour_personalize_meta['personalize_title3']) ?>
                </span>
                <div class="clear0"></div>
                <span class="text alt"></span>
            </div>
        </div>

        <div class="clear5"></div>

        <div class="container small text">
           <?php echo apply_filters('the_content', $lamour_personalize_content) ?>
        </div>

        <div class="clear5"></div>

    </div>
</div>


<!--<div class="section padding whiteBG">-->
<!--    <div class="container textCenter">-->
<!--        <div class="title-script">--><?php //echo esc_html($lamour_section_title) ?><!--</pp></div>-->
<!--        <div class="clear5"></div>-->
<!---->
<!--        <div class="promo-section">-->
<!--            --><?php //if(isset($lamour_section_meta['personalize_title1'])): ?>
<!--                <div class="trip">-->
<!--                    <img src="--><?php //echo esc_url($lamour_personalize_image1[0]) ?><!--" />-->
<!--                    <div class="clear0"></div>-->
<!--                    <span class="subtitle grey">--><?php //echo esc_html($lamour_section_meta['personalize_title1']) ?><!--</span>-->
<!--                    <div class="clear0"></div>-->
<!--                    <span class="text alt">--><?php //echo esc_html($lamour_section_meta['personalize_description1']) ?><!--</span>-->
<!--                </div>-->
<!--            --><?php //endif ?>
<!---->
<!--            --><?php //if(isset($lamour_section_meta['personalize_title2'])): ?>
<!--                <div class="trip">-->
<!--                    <img src="--><?php //echo esc_url($lamour_personalize_image2[0]) ?><!--" />-->
<!--                    <div class="clear0"></div>-->
<!--                    <span class="subtitle grey">--><?php //echo esc_html($lamour_section_meta['personalize_title2']) ?><!--</span>-->
<!--                    <div class="clear0"></div>-->
<!--                    <span class="text alt">--><?php //echo esc_html($lamour_section_meta['personalize_description2']) ?><!--</span>-->
<!--                </div>-->
<!--            --><?php //endif ?>
<!--            --><?php //if(isset($lamour_section_meta['personalize_title3'])): ?>
<!--                <div class="trip">-->
<!--                    <img src="--><?php //echo esc_url($lamour_personalize_image3[0]) ?><!--" />-->
<!--                    <div class="clear0"></div>-->
<!--                    <span class="subtitle grey">--><?php //echo esc_html($lamour_section_meta['personalize_title3']) ?><!--</span>-->
<!--                    <div class="clear0"></div>-->
<!--                    <span class="text alt">--><?php //echo esc_html($lamour_section_meta['personalize_description3']) ?><!--</span>-->
<!--                </div>-->
<!--            --><?php //endif ?>
<!---->
<!--        </div>-->
<!---->
<!--    </div>-->
<!--</div>-->