<?php
global $lamour_section_id;

$lamour_plain         = get_post($lamour_section_id);
$lamour_plain_content = $lamour_plain->post_content;
$lamour_plain_content = apply_filters('the-content', $lamour_plain_content)

?>
<div class="section padding whiteBG border-top">
    <div class="container textCenter">
        <div class="container medium text textLeft">
            <?php echo wp_kses_post($lamour_plain_content) ?>
        </div>

    </div>
</div>
