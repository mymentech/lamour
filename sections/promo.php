<?php
global $lamour_section_id;
$lamour_section_meta = get_post_meta( $lamour_section_id, 'lamour-section-promo', true );


$lamour_promo_image1 = get_template_directory_uri() . '/assets/images/promo_triangle.png';
$lamour_promo_image2 = get_template_directory_uri() . '/assets/images/promo_cuffs.png';
$lamour_promo_image3 = get_template_directory_uri() . '/assets/images/promo_shoe.png';

if ( isset( $lamour_section_meta['promo_image1'] ) ) {
    $lamour_promo_image1 = wp_get_attachment_image_src( $lamour_section_meta['promo_image1'], 'full' )[0];
}
if ( isset( $lamour_section_meta['promo_image2'] ) ) {
    $lamour_promo_image2 = wp_get_attachment_image_src( $lamour_section_meta['promo_image2'], 'full' )[0];
}
if ( isset( $lamour_section_meta['promo_image3'] ) ) {
    $lamour_promo_image3 = wp_get_attachment_image_src( $lamour_section_meta['promo_image3'], 'full' )[0];
}


$lamour_section             = get_post( $lamour_section_id );
$lamour_section_title       = $lamour_section->post_title;
$lamour_section_description = $lamour_section->post_content;



?>
<div class="section padding whiteBG">
    <div class="container textCenter">
        <div class="title-script"><?php echo esc_html($lamour_section_title) ?></pp></div>
        <div class="clear5"></div>

        <div class="promo-section">
            <?php if(isset($lamour_section_meta['promo_title1'])): ?>
            <div class="trip">
                <img src="<?php echo esc_url($lamour_promo_image1) ?>" />
                <div class="clear0"></div>
                <span class="subtitle grey"><?php echo esc_html($lamour_section_meta['promo_title1']) ?></span>
                <div class="clear0"></div>
                <span class="text alt"><?php echo esc_html($lamour_section_meta['promo_description1']) ?></span>
            </div>
            <?php endif ?>

            <?php if(isset($lamour_section_meta['promo_title2'])): ?>
            <div class="trip">
                <img src="<?php echo esc_url($lamour_promo_image2) ?>" />
                <div class="clear0"></div>
                <span class="subtitle grey"><?php echo esc_html($lamour_section_meta['promo_title2']) ?></span>
                <div class="clear0"></div>
                <span class="text alt"><?php echo esc_html($lamour_section_meta['promo_description2']) ?></span>
            </div>
            <?php endif ?>
            <?php if(isset($lamour_section_meta['promo_title3'])): ?>
            <div class="trip">
                <img src="<?php echo esc_url($lamour_promo_image3) ?>" />
                <div class="clear0"></div>
                <span class="subtitle grey"><?php echo esc_html($lamour_section_meta['promo_title3']) ?></span>
                <div class="clear0"></div>
                <span class="text alt"><?php echo esc_html($lamour_section_meta['promo_description3']) ?></span>
            </div>
            <?php endif ?>

        </div>

    </div>
</div>