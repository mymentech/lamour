<?php
global $lamour_section_id;

$lamour_subscription_meta = get_post_meta( $lamour_section_id, 'lamour-section-subscription', true );

$lamour_subscription_bg = get_template_directory_uri() . '/assets/images/bg_ornament.jpg';

if(isset($lamour_subscription_meta['subscription_bg_image'])){
    $lamour_subscription_bg = wp_get_attachment_image_url($lamour_subscription_meta['subscription_bg_image'],'full');
}

$lamour_subscription_image = get_the_post_thumbnail_url($lamour_section_id,'full');




$lamour_subscription            = get_post($lamour_section_id );
$lamour_subscription_title       = $lamour_subscription->post_title;
$lamour_subscription_content = $lamour_subscription->post_content;



?>
<div class="section whiteBG bgImg subscribe-header" style="background-image:url('<?php echo esc_url($lamour_subscription_bg) ?>');">
    <div class="fullSection"><div class="fullSectionContent">
            <div class="container textLeft">
                <div class="floatL half small">
                    <img src="<?php echo esc_url($lamour_subscription_image) ?>"/>

                </div>

                <div class="half large textLeft floatL">
                    <span class="text-script red"><?php echo esc_html($lamour_subscription_title) ?></span>
                    <div class="clear"></div>
                    <span class="text">
                        <?php echo apply_filters('the_content', $lamour_subscription_content); ?>
                    </span>

                    <div class="clear2"></div>
                    <a href="<?php if(isset($lamour_subscription_meta['button_url'])) echo esc_url($lamour_subscription_meta['button_url']) ?>">
                        <div class="button primary">
                            <?php if(isset($lamour_subscription_meta['button_title'])) echo esc_html($lamour_subscription_meta['button_title']) ?>
                        </div>
                    </a>
                </div>
            </div>
        </div></div>
</div>