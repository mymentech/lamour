<?php
global $lamour_section_id;
$witb_section             = get_post($lamour_section_id);
$witb_section_title       = $witb_section->post_title;
$witb_section_content = $witb_section->post_content;


?>

<div class="section padding redBG">
    <div class="container small textCenter">
        <span class="title-script white"><?php echo esc_html($witb_section_title) ?></span>
        <div class="clear2"></div>
        <i class="fa fa-question-circle white icon-large"></i>
        <div class="clear2"></div>
        <span class="text white">
            <?php echo wp_kses_post($witb_section_content) ?>
        </span>
    </div>
</div>
