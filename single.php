

<?php

get_header();


?>


<?php

while(have_posts()):the_post();


?>

<div class="section noPadding whiteBG bgImg blog-head" id="page-header" style="background-image:url(<?php the_post_thumbnail_url(); ?>);">
	<div class="overlay" style="background-color: rgba(60, 60, 60, 0.6);"></div>
	<div class="fullSection">
		<div class="fullSectionContent">
			<div class="container small textCenter">
				<span class="subtitle white"><?php _e('The Lamour Blog', 'lamour') ?></span>
				<div class="clear2"></div>
				<span class="title-script white"><?php the_title(); ?></span>
				<div class="clear3"></div>
                <h1 class="text white hide" style="text-transform: uppercase";><?php the_time('F, m, Y') ?></h1>
			</div>
		</div>
	</div>
</div>

<div class="section padding whiteBG border-top" id="blog-info">
	<div class="container textCenter">

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-503474450a6821f3"></script>
	<div class="addthis_sharing_toolbox textCenter"></div>
	
	<div class="clear5"></div>
	
	<div class="container full text textLeft">
      
      <div class="content-large">

          <?php the_content(); ?>

      	<div class="clear5"></div>




      <div id="disqus_thread"></div>
		<script type="text/javascript">
	        var disqus_shortname = 'lamour-secret';
			var disqus_identifier = 'lamour-secret_21';
	
	        (function() { // DON'T EDIT BELOW THIS LINE
	        	var d = document, s = d.createElement('script');
	        	s.src = '//lamour-secret.disqus.com/embed.js';
	        	s.setAttribute('data-timestamp', +new Date());
	        	(d.head || d.body).appendChild(s);
	        	})();
	    </script>
      </div>



       <!-- sidebar start -->

        <?php dynamic_sidebar('single-post-right'); ?>

        <!-- sidebar end-->


        
    
    </div>
 

                                

	</div>
</div>

<?php endwhile; ?>





<?php get_footer(); ?>