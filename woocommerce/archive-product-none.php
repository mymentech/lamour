<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;
get_header();
?>

    <div class="section whiteBG">
        <div class="container">

            <div class="shop left">
                <div class="shop-box">
                    <div class="subtitle">Shop</div>
                    <!--<div class="title alt">119</div>-->
                    <div class="subtitle alt">All Products</div>

                    <div class="clear"></div>
                    <div id="filter">
                        <select class="chzn-select chzn-done" id="sort_products" onchange="window.location='http://lamoursecret.com/shop/'+$(this).val();" style="display: none;">
                            <option value="">Sort by most recent</option>
                            <option value="?by=name/asc">Name - A to Z</option>
                            <option value="?by=name/desc">Name - Z to A</option>
                            <option value="?by=price/asc">Price - Low to High</option>
                            <option value="?by=price/desc">Price - High to Low</option>
                        </select>
                        <div id="sort_products_chzn" class="chzn-container chzn-container-single chzn-container-single-nosearch" style="width: 210px;"><a href="javascript:void(0)" class="chzn-single"><span>Sort by most recent</span><div><b></b></div></a><div class="chzn-drop" style="left: -9000px; width: 210px; top: 34px;"><div class="chzn-search"><input type="text" autocomplete="off" style="width: 175px;"></div><ul class="chzn-results"><li id="sort_products_chzn_o_0" class="active-result result-selected" style="">Sort by most recent</li><li id="sort_products_chzn_o_1" class="active-result" style="">Name - A to Z</li><li id="sort_products_chzn_o_2" class="active-result" style="">Name - Z to A</li><li id="sort_products_chzn_o_3" class="active-result" style="">Price - Low to High</li><li id="sort_products_chzn_o_4" class="active-result" style="">Price - High to Low</li></ul></div></div>
                    </div>
                </div>

                <div class="clear2"></div>

                <div class="shop-box lightBG">
                    <span class="text-script grey2">Categories</span>
                    <div class="clear2"></div>

                    <a href="/shop"><span class="text alt default">All Products</span></a>
                    <div class="clear0"></div>
                    <a href="/shop/best-seller"><span class="text alt default">Best Seller</span></a>
                    <div class="clear0"></div>
                    <a href="/shop/bondage-and-fetish"><span class="text alt default">Bondage and Fetish</span></a>
                    <div class="clear0"></div>
                    <a href="/shop/for-her"><span class="text alt default">For Her</span></a>
                    <div class="clear0"></div>
                    <a href="/shop/for-him"><span class="text alt default">For Him</span></a>
                    <div class="clear0"></div>
                    <a href="/shop/furniture"><span class="text alt default">Furniture</span></a>
                    <div class="clear0"></div>
                    <a href="/shop/kegel-exercises"><span class="text alt default">Kegel Exercises</span></a>
                    <div class="clear0"></div>
                    <a href="/shop/lingerie"><span class="text alt default">Lingerie</span></a>
                    <div class="clear0"></div>
                    <a href="/shop/lubricants"><span class="text alt default">Lubricants</span></a>
                    <div class="clear0"></div>
                    <a href="/shop/miscellaneous"><span class="text alt default">Miscellaneous</span></a>
                    <div class="clear0"></div>
                    <a href="/shop/rabbits"><span class="text alt default">Rabbits</span></a>
                    <div class="clear0"></div>

                </div>
            </div>

            <div class="shop right">
                <?php
                // Only run on shop archive pages, not single products or other pages
                if ( is_shop() || is_product_category() || is_product_tag() ) {
                    // Products per page
                    $per_page = -1;
                    $lamour_lc = 0;
                    if ( get_query_var( 'taxonomy' ) ) { // If on a product taxonomy archive (category or tag)
                        $args = array(
                            'post_type' => 'product',
                            'posts_per_page' => $per_page,
                            'paged' => get_query_var( 'paged' ),
                            'tax_query' => array(
                                array(
                                    'taxonomy' => get_query_var( 'taxonomy' ),
                                    'field'    => 'slug',
                                    'terms'    => get_query_var( 'term' ),
                                ),
                            ),
                        );
                    } else { // On main shop page
                        $args = array(
                            'post_type' => 'product',
                            'orderby' => 'date',
                            'order' => 'DESC',
                            'posts_per_page' => $per_page,
                            'paged' => get_query_var( 'paged' ),
                        );
                    }
                    // Set the query
                    $products = new WP_Query( $args );
                    // Standard loop
                    if ( $products->have_posts() ) :
                        while ( $products->have_posts() ) : $products->the_post();

                        $lamour_product = wc_get_product(get_the_ID());

                        $rp = $lamour_product->get_price();

                    ?>
                            <div class="product-item">
                                <a href="<?php the_permalink() ?>">
                                    <img src="<?php echo get_theme_file_uri("/assets/images/blank.png") ?>" style="background-image:url('<?php echo get_the_post_thumbnail_url(get_the_ID(),'full')?>');" class="img">
                                </a>

                                <div class="shop-info">
                                    <span class="text"><?php the_title() ?></span>
                                    <div class="clear0"></div>
                                    <a href="<?php the_permalink() ?>">
                                        <span class="price grey2"><?php lamour_the_product_price($lamour_product) ?></span>
                                    </a>
                                    <div class="clear0"></div>
                                </div>

                            </div>
                        <?php $lamour_lc++ ?>
                        <?php
                            if($lamour_lc%3==0){
                                printf('<div class="clear3"></div>');
                            }
                            ?>

                        <?php
                        endwhile;
                        wp_reset_postdata();
                    endif;
                } else { // If not on archive page (cart, checkout, etc), do normal operations
                    woocommerce_content();
                }
                ?>



            </div>

        </div>
    </div>

<?php
get_footer();
