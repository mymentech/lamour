<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header();

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>

<?php
/**
 * Hook: woocommerce_archive_description.
 *
 * @hooked woocommerce_taxonomy_archive_description - 10
 * @hooked woocommerce_product_archive_description - 10
 */
do_action( 'woocommerce_archive_description' );
?>

<?php
$lamour_shop_page_url = esc_url( get_permalink( wc_get_page_id( 'shop' ) ) );
$lamour_product_cats  = lamour_get_wc_categories();

?>

    <div class="section whiteBG" id="lemourshop">
        <div class="container">
            <div class="shop left">
                <div class="shop-box">
                    <div class="subtitle"><?php _e( "Shop", "lamour" ) ?></div>
                    <!--<div class="title alt">119</div>-->
                    <div class="subtitle alt"><?php _e( "All Products", "lamour" ) ?></div>

                    <div class="clear"></div>
                    <div id="filter">
						<?php do_action( 'lamour_woocommerce_before_shop_loop' ); ?>
                    </div>
                </div>

                <div class="clear2"></div>

                <div class="shop-box lightBG">
                    <span class="text-script grey2">Categories</span>
                    <div class="clear2"></div>
                    <a href="<?php echo $lamour_shop_page_url ?>"><span
                                class="text alt default"><?php _e( "All Products", "lamour" ) ?></span></a>
                    <div class="clear0"></div>
					<?php
					foreach ( $lamour_product_cats as $category ) {
						$cat_id    = $category->term_id;
						$cat_title = $category->name;
						$cat_url   = esc_url( get_category_link( $cat_id ) );
						printf( "<a href='%s'><span class='text alt default'>%s</span></a>", $cat_url, $cat_title );
						printf(" <div class=\"clear0\"></div>");


					}
					?>

                </div>
            </div>
            <div class="shop right">
				<?php
				if ( woocommerce_product_loop() ) {
					$lamour_lc = 0;

					/**
					 * Hook: woocommerce_before_shop_loop.
					 *
					 * @hooked wc_print_notices - 10
					 * @hooked woocommerce_result_count - 20
					 * @hooked woocommerce_catalog_ordering - 30
					 */
					do_action( 'woocommerce_before_shop_loop' );

					woocommerce_product_loop_start();

					if ( wc_get_loop_prop( 'total' ) ) {
						while ( have_posts() ) {
							the_post();

							/**
							 * Hook: woocommerce_shop_loop.
							 *
							 * @hooked WC_Structured_Data::generate_product_data() - 10
							 */
							do_action( 'woocommerce_shop_loop' );

							wc_get_template_part( 'content', 'product' );
							$lamour_lc ++;

							if ( $lamour_lc % 3 == 0 ) {
								printf( '<div class="clear3"></div>' );
							}

						}
					}

					woocommerce_product_loop_end();

					/**
					 * Hook: woocommerce_after_shop_loop.
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
				} else {
					/**
					 * Hook: woocommerce_no_products_found.
					 *
					 * @hooked wc_no_products_found - 10
					 */
					do_action( 'woocommerce_no_products_found' );
				}

				/**
				 * Hook: woocommerce_after_main_content.
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				do_action( 'woocommerce_after_main_content' );

				/**
				 * Hook: woocommerce_sidebar.
				 *
				 * @hooked woocommerce_get_sidebar - 10
				 */
				do_action( 'woocommerce_sidebar' );
				?>

            </div>
        </div>
    </div>


<?php
get_footer();
